# game_of_life

A Flutter Game of Life project.

## Getting Started

The game of life is an automaton that animates "cells". Cells are either alive or dead.

If a cell has exactly three living neighbors, it is alive at the next stage,

if a cell has exactly two living neighbors, it remains in its current state at the next stage,

if a cell has strictly less than two or strictly more than three living neighbors, it is dead at the next stage.

Neighbors are the cells surrounding the cell in question.

The game takes shape on a "grid" (of which you define a fairly large size), a pattern of cells is defined in advance, then a number of cycles is played (from 1 to infinity) and finally a pattern of cells is expected throughout the cycles.

It can be tested on web browsers at:
<https://stephan-flutter-game-of-life.netlify.app/#/>

The apk file can be downloaded from:
<https://drive.google.com/file/d/1H_-dwBFqKeVU3jyZfyezEBCuIng2F4xO/view?usp=sharing>
