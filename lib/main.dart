import 'package:flutter/material.dart';

void main() {
  runApp(const GameOfLifeApp());
}

class GameOfLifeApp extends StatelessWidget {
  const GameOfLifeApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Game of Life',
      theme: ThemeData(
        useMaterial3: true,
        primarySwatch: Colors.blue,
      ),
      home: const GameOfLifeScreen(),
    );
  }
}

class GameOfLifeScreen extends StatefulWidget {
  const GameOfLifeScreen({super.key});

  @override
  _GameOfLifeScreenState createState() => _GameOfLifeScreenState();
}

class _GameOfLifeScreenState extends State<GameOfLifeScreen> {
  final int rows = 30;
  final int cols = 30;
  List<List<bool>> grid = [];

  @override
  void initState() {
    super.initState();
    initializeGrid();
    startGame();
  }

  void initializeGrid() {
    grid = List.generate(rows, (_) => List.generate(cols, (_) => false));
    // initial live cells
    grid[10][10] = true;
    grid[11][10] = true;
    grid[12][10] = true;
  }

  void startGame() {
    Future.delayed(const Duration(seconds: 1), () {
      evolve();
      startGame();
    });
  }

  void evolve() {
    List<List<bool>> newGrid =
        List.generate(rows, (_) => List.generate(cols, (_) => false));

    for (int row = 0; row < rows; row++) {
      for (int col = 0; col < cols; col++) {
        int liveNeighbors = countLiveNeighbors(row, col);

        if (grid[row][col]) {
          if (liveNeighbors == 2 || liveNeighbors == 3) {
            newGrid[row][col] = true;
          }
        } else {
          if (liveNeighbors == 3) {
            newGrid[row][col] = true;
          }
        }
      }
    }

    setState(() {
      grid = newGrid;
    });
  }

  int countLiveNeighbors(int row, int col) {
    int count = 0;
    for (int i = -1; i <= 1; i++) {
      for (int j = -1; j <= 1; j++) {
        // Skip the current cell
        if (i == 0 && j == 0) continue;
        int newRow = row + i;
        int newCol = col + j;
        if (newRow >= 0 && newRow < rows && newCol >= 0 && newCol < cols) {
          if (grid[newRow][newCol]) {
            count++;
          }
        }
      }
    }
    return count;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Game of Life'),
      ),
      body: Center(
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: cols,
          ),
          itemBuilder: (context, index) {
            int row = index ~/ cols;
            int col = index % cols;
            return GestureDetector(
              onTap: () {
                setState(() {
                  grid[row][col] = !grid[row][col];
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  color: grid[row][col] ? Colors.blue : Colors.white,
                ),
              ),
            );
          },
          itemCount: rows * cols,
        ),
      ),
    );
  }
}
